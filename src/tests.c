#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#define HEAP_SIZE 16384
#define BLOCK_SIZE 1024
#define SMALL_HEAP_SIZE 32
#define VERY_SMALL_HEAP_SIZE 1

static inline void destroy_heap(void* heap, size_t size) {
  munmap(heap, size_from_capacity((block_capacity) {.bytes = size}).bytes);
}

void test1(void) {
  printf("---TEST1---\n Successfull malloc\n");
  void* heap = heap_init((size_t) HEAP_SIZE);
  printf("Heap after initialization\n");
  debug_heap(stdout, heap);

  void* mem = _malloc((size_t) 4*BLOCK_SIZE);
  printf("Heap after malloc\n");
  debug_heap(stdout, heap);

  if (!heap || !mem) {
    fprintf(stderr, "---TEST1 FAILED----\n\n");
    return;
  }

  _free(mem);
  printf("Heap after freeing\n");
  debug_heap(stdout, heap);
 
  destroy_heap(heap, (size_t) HEAP_SIZE);
  printf("---TEST1 PASSED---\n\n");

}

void test2(void) {
  printf("---TEST2---\n Successfull multiple malloc and free 1\n");
  void* heap = heap_init((size_t) HEAP_SIZE);
  printf("Heap after initialization\n");
  debug_heap(stdout, heap);

  void* mem1 = _malloc((size_t) 4*BLOCK_SIZE);
  printf("Heap after 1malloc\n");
  debug_heap(stdout, heap);
  
  void* mem2 = _malloc((size_t) BLOCK_SIZE);
  printf("Heap after 2malloc\n");     
  debug_heap(stdout, heap); 

  if (!heap || !mem1 || !mem2) {
    fprintf(stderr, "---TEST2 FAILED---\n\n");
    return;
  }

  _free(mem1);
  printf("Heap after freeing mem1\n");
  debug_heap(stdout, heap);

  destroy_heap(heap, (size_t) HEAP_SIZE);
  printf("---TEST2 PASSED---\n\n");
 
}


void test3(void) {
  printf("---TEST3---\n Successfull multiple malloc and free 1\n");
  void* heap = heap_init((size_t) HEAP_SIZE);                          
  printf("Heap after initialization\n");                           
  debug_heap(stdout, heap);                                                
                                                                   
  void* mem1 = _malloc((size_t) 4*BLOCK_SIZE);                             
  printf("Heap after 1malloc\n");                                  
  debug_heap(stdout, heap);                                                
                                                                   
  void* mem2 = _malloc((size_t) BLOCK_SIZE);                             
  printf("Heap after 2malloc\n");                                  
  debug_heap(stdout, heap);

  void* mem3 = _malloc((size_t) 2*BLOCK_SIZE);
  printf("Heap after 3malloc\n");     
  debug_heap(stdout, heap);                   
                                                                   
  if (!heap || !mem1 || !mem2 || !mem3) {                                   
    fprintf(stderr, "---TEST3 FAILED----\n\n");                        
    return;                                                        
  }                                                                
                                                                   
  _free(mem1);                                                     
  printf("Heap after freeing mem1\n");                             
  debug_heap(stdout, heap);                                                

  _free(mem3);
  printf("Heap after freeing mem3\n");                             
  debug_heap(stdout, heap);
  
  destroy_heap(heap, (size_t) HEAP_SIZE);                              
  printf("---TEST3 PASSED---\n\n");                                    
 
}


void test4(void) {
  printf("---TEST4---\n Growing heap\n");
  void* heap = heap_init((size_t) SMALL_HEAP_SIZE);
  printf("Heap after initialization\n");
  debug_heap(stdout, heap);

  void* mem1 = _malloc((size_t) 4*BLOCK_SIZE);
  printf("Heap after 1malloc\n");
  debug_heap(stdout, heap);

  void* mem2 = _malloc((size_t) 4*BLOCK_SIZE);
  printf("Heap after 2malloc\n");      
  debug_heap(stdout, heap);                     

  if (!heap || !mem1 || !mem2 || !block_get_header(mem1) || block_get_header(mem1)->next != block_get_header(mem2)) {
    fprintf(stderr, "---TEST4 FAILED---\n\n");
    return;
  }

  _free(mem1);
  printf("Heap after 1freeing\n");
  debug_heap(stdout, heap);

  _free(mem2);
  printf("Heap after 2freeing\n");
  debug_heap(stdout, heap);
  
  destroy_heap(heap, (size_t) HEAP_SIZE);
  printf("---TEST4 PASSED---\n\n");

}

void test5(void){
  printf("---TEST5---\n Oh...\n");
  void* heap = heap_init((size_t) VERY_SMALL_HEAP_SIZE);
  printf("Heap after initialization\n");
  debug_heap(stdout, heap);

  void* mem1 = _malloc((size_t) 4*BLOCK_SIZE);
  printf("Heap after 1malloc\n");
  debug_heap(stdout, heap);


  struct block_header* head1 = block_get_header(mem1);

  if (!heap || !mem1) {
    fprintf(stderr, "---TEST5 FAILED---\n\n");
    return;
  }

  (void) mmap(head1->contents + head1->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

  
  void* mem2 = _malloc((size_t) 4*BLOCK_SIZE);
  printf("Heap after 2malloc\n");      
  debug_heap(stdout, heap);                     


  if (!mem2) {
    fprintf(stderr, "---TEST5 FAILED---\n\n");
    return;
  }
  
  _free(mem1);
  printf("Heap after 1freeing\n");
  debug_heap(stdout, heap);

  _free(mem2);
  printf("Heap after 2freeing\n");
  debug_heap(stdout, heap);
  
  destroy_heap(heap, (size_t) HEAP_SIZE);
  printf("---TEST5 PASSED---\n\n");
}

void run_tests(void){
  test1();
  test2();
  test3();
  test4();
  test5();
}
